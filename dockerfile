FROM python:3.8-slim
RUN mkdir /app
WORKDIR /app
RUN apt-get update
RUN apt-get install python3 -y
COPY . .
RUN pip install -r requirements.txt

WORKDIR /app/pagina-python 
CMD ["python3", "main.py"]